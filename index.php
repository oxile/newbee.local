<?php
require_once __DIR__ . '/vendor/autoload.php';

$url = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

$urlParts = explode('/', $url);

$controller = !empty($urlParts[1]) ? ucfirst($urlParts[1]) : 'Default';
$action = !empty($urlParts[2]) ? ucfirst($urlParts[2]) : 'All';

$controllerClass = 'App\controllers\\' . $controller . 'Controller';

if(!class_exists($controllerClass)) {
    throw new Exception('Undefined controller');
}
$controller = new $controllerClass;
$method = 'action' . $action;

if(!method_exists($controller, $method)) {
    throw new Exception('Undefined method');
}

$controller->$method();

/*
форма авторизации
обработка формы
вывод ошибок
главная страница для гостей
выход с сайта
хэш
*/