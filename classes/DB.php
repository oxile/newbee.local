<?php

namespace App\classes;

use PDO;

/**
 * Class DB
 * @var DB $dbh
 */
class DB
{
    private $dbh; // сохранение свойства для дальнейшего использования в запросах
    private $className = 'stdClass';

    public function __construct() // создание объекта класса PDO
    {
        $dsn = 'mysql:dbname=newbee.local;host=localhost';
        $username = 'root';
        $password = '';
        $this->dbh = new PDO($dsn, $username, $password);
    }

    public function setClassName($className)
    {
        $this->className = $className;
    }

    public function query($sql, $params = [])
    {
        $sth = $this->dbh->prepare($sql); // create statement with help method prepare (prepare sql)
        $sth->execute($params); // execute sql with params
        return $sth->fetchAll(PDO::FETCH_CLASS, $this->className); // return result of executed sql
    }

    public function execute($sql, $params = [])
    {
        $sth = $this->dbh->prepare($sql);
        $sth->execute($params);
    }

    public function lastInsertId()
    {
        return $this->dbh->lastInsertId();
    }
}