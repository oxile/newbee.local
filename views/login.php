<?php

use App\models\Users;



if (Users::checkLoginPassword($login, $password)) {
    header('Location: /views/login.php');
    exit;
}

header('Location: /index.php');
exit;