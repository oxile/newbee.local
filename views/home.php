<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://kit.fontawesome.com/31f798355e.js" crossorigin="anonymous"></script>
    <title>Home</title>
</head>

<body>
    <header>
        <div class="container">
            <nav class="navbar navbar-expand-lg navbar-dark">
                <a class="navbar-brand" href="#">OXILE</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mb-2 mb-lg-0" id="list-menu">
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="/">Главная</a></li>
                        <li class="nav-item"><a class="nav-link" aria-current="page" href="/user/logout">Выйти</a></li>
                    </ul>
                </div>
            </nav>
        </div>
    </header>
    <div class="container slider">
        <div id="slider" class="carousel slide carousel-fade" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#slider" data-slide-to="0" class="active"></li>
                <li data-target="#slider" data-slide-to="1"></li>
                <li data-target="#slider" data-slide-to="2"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img src="../img/header-img.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="../img/header-img2.jpg" class="d-block w-100" alt="...">
                </div>
                <div class="carousel-item">
                    <img src="../img/header-img3.jpg" class="d-block w-100" alt="...">
                </div>
            </div>
            <a class="carousel-control-prev" href="#slider" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#slider" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col">
                <div class="single-post">
                    <h3 class="post-category">История</h3>
                    <h2 class="post-title"><a href="#">Toyota Sprinter Trueno, Toyota Corolla Levin</a></h2>
                    <p>Cерия компактных купе фирмы Toyota, производившиеся на базе автомобилей Toyota Corolla / Toyota Sprinter.</p>
                    <p>Серия AE110, AE111 — Май 1995 — Июль 2000 4 поколение Toyota Corolla Levin и Sprinter Trueno, выпущенное в 1995 году, имело более угловатый дизайн — в полном соответствии с изменениями, произошедшими в дизайне всех автомобилей Toyota в середине 90-х. Однако общая концепция была унаследована от предыдущих поколений модели. Так же, как и ранее, отличие Corolla Levin от Sprinter Trueno заключалось в разном дизайне передней и задней оптики и наличии/отсутствии решетки радиатора.</p>
                    <p><a href="#" class="link-more">Оставить комментарий</a></p>
                </div>
            </div>
        </div>
    </div>
    
    <div class="grid-posts">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img src="../img/thumb1.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h3 class="post-category">Внешний вид</h3>
                            <h5 class="post-title">Задние стопсигналы Trueno</h5>
                            <p class="card-text">Стоп сигналы такого вида устанавливались на Toyota Sprinter Trueno AE111 в рестайлинг версии с 1997 по 2000 года.</p>
                            <a href="#" class="btn btn-primary">Перейти к посту</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img src="../img/thumb2.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h3 class="post-category">Внешний вид</h3>
                            <h5 class="post-title">Передняя губа Dog Fight</h5>
                            <p class="card-text">Такая губа на передний бампер производилась в Японии в тюнинг ателье и имела две вариации для установки на Trueno и Levin бампера.</p>
                            <a href="#" class="btn btn-primary">Перейти к посту</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card" style="width: 18rem;">
                        <img src="../img/thumb3.jpg" class="card-img-top" alt="...">
                        <div class="card-body">
                            <h3 class="post-category">Погода</h3>
                            <h5 class="post-title">Снег</h5>
                            <p class="card-text">Этой зимой было достаточно холодно.</p>
                            <a href="#" class="btn btn-primary">Перейти к посту</a>
                        </div>
                    </div>
                </div>
                <div class="col text-center after-posts">
                    <button type="button" class="btn btn-outline-secondary load-more">
                        Load more
                        <span class="spinner-grow spinner-grow-sm" role="status" aria-hidden="true"></span>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <section class="signup">
        <div class="container">
            <div class="row">
                <div class="col">
                    <h3 class="text-center">Sign up for our newsletter.</h3>
                    <form>
                        <div class="form-row">
                            <div class="col-lg-4 offset-lg-4 col-md-6 offset-md-3 form-input">
                                <label>
                                    <input type="email" class="form-control" placeholder="Enter a valid email address" required="">
                                </label>
                                <button class="btn" type="submit">&#10146;</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

<?php require_once __DIR__ . '/footer.php'; ?>

    <script src="../js/jquery-3.6.0.min.js"></script>
    <script src="../js/popper.min.js"></script>
    <script src="../bootstrap/js/bootstrap.min.js"></script>
    <script src="../js/main.js"></script>
</body>

</html>