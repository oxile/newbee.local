<?php

function logout()
{
    unset($_COOKIE["auth"]);
    setcookie("auth", "", time()-8640000, "/");
}

logout();
header('Location: /index.php');
exit;