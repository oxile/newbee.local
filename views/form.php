<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,600;0,700;0,800;1,300;1,400;1,600;1,700;1,800&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/style.css">
    <script src="https://kit.fontawesome.com/31f798355e.js" crossorigin="anonymous"></script>
    <title>Authorization</title>
</head>

<body>
<header>
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-dark">
            <a class="navbar-brand" href="#">OXILE</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto mb-2 mb-lg-0" id="list-menu">
                    <li class="nav-item"><a class="nav-link" aria-current="page" href="/">Главная</a></li>
                    <li class="nav-item"><a class="nav-link" aria-current="page" href="#">News</a></li>
                    <li class="nav-item"><a class="nav-link" aria-current="page" href="#">About us</a></li>
                    <li class="nav-item"><a class="nav-link" aria-current="page" href="#">Contact</a></li>
                </ul>
            </div>
        </nav>
    </div>
</header>
<div class="container col-md-4 alert alert-light" role="alert">
    <h4>
        Добро пожаловать!
        <br>
        Для продолжения работы на сайте необходима авторизация.
    </h4>
</div>
<div class="container col-md-4 shadow p-3 mb-5 bg-white rounded">
    <form action="/user/login" method="post">
        <div class="form-group">
            <label for="exampleInputEmail1">Логин</label>
            <input type="text" name="login" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp">
            <small id="emailHelp" class="form-text text-muted">Мы никогда никому не передадим ваши данные</small>
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Пароль</label>
            <input type="password" name="password" class="form-control" id="exampleInputPassword1">
        </div>
        <div class="form-group form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1" name="remember">
            <label class="form-check-label" for="exampleCheck1">Запомнить меня</label>
        </div>
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>
<?php require_once __DIR__ . '/footer.php'; ?>

<script src="../js/jquery-3.6.0.min.js"></script>
<script src="../js/popper.min.js"></script>
<script src="../bootstrap/js/bootstrap.min.js"></script>
<script src="../js/main.js"></script>
</body>

</html>