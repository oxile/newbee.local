<?php


namespace App\controllers;

use App\classes\View;

class DefaultController
{
    public function actionAll()
    {
        $items = '';
        $view = new View();
        $view->assign('posts', $items);
        $view->display('home.php');
    }

    public function actionForm()
    {
        $view = new View();
        $view->display('form.php');
    }

}