<?php

namespace App\controllers;

class UserController
{
    public function actionLogin()
    {
        setcookie('auth', $_POST['login'], time()+86400, '/');
        header('Location: /');
    }

    public function actionLogout()
    {
        unset($_COOKIE);
        setcookie('auth', '', time()-86400, '/');
        header('Location: /../views/form.php');
    }

}